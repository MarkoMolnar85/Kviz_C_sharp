﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
namespace Tijana_Jakovljevic_klase
{
    class SvaPitanjaIodgovori
    {
        public static string TacanOdg;
        int ukupanBrojPitanja;
        Random rand = new Random(DateTime.Now.Millisecond);
        List<string> ListaSaSvimPitanjeima;
        List<string> ListaSvihTacnihOdgovora;
        List<string> ListaSvihNetacnih1;
        List<string> ListaSvihNetacnih2;
        public static int brojIzabranihPitanja = 4;
        public SvaPitanjaIodgovori()
        {
            ListaSaSvimPitanjeima = new List<string>(30);
            ListaSvihTacnihOdgovora = new List<string>(30);
            ListaSvihNetacnih1 = new List<string>(30);
            ListaSvihNetacnih2 = new List<string>(30);
        }

        public void PrebrojPitanjaIPopuniListe(Excel.Range domet)
        {
            ukupanBrojPitanja = 0;
            int red = 1;
            while (domet.Cells[red, 1].Value2 != null)
            {
                ListaSaSvimPitanjeima.Add(domet.Cells[red, 1].Value2.ToString());
                ListaSvihTacnihOdgovora.Add(domet.Cells[red, 2].Value2.ToString());
                ListaSvihNetacnih1.Add(domet.Cells[red, 3].Value2.ToString());
                ListaSvihNetacnih2.Add(domet.Cells[red, 4].Value2.ToString());
                ukupanBrojPitanja++;
                red++;
            }
        }

        public void PredajPitanjeIodg(PitanjaOdgovori po)
        {
            int indeks = 0;
            do
            {
                indeks = rand.Next(1, ukupanBrojPitanja);
            } while (ListaSaSvimPitanjeima[indeks] == null);

            po.Pitanje = ListaSaSvimPitanjeima[indeks];
            po.UvekTacan = ListaSvihTacnihOdgovora[indeks];
            po.Odg1 = ListaSvihTacnihOdgovora[indeks];
            po.Odg2 = ListaSvihNetacnih1[indeks];
            po.Odg3 = ListaSvihNetacnih2[indeks];
            ListaSaSvimPitanjeima[indeks] = null;
        }
        

    }
}
