﻿namespace Tijana_Jakovljevic_klase
{
    partial class frmMilioner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bntNovaIgra = new System.Windows.Forms.Button();
            this.lblPitanje = new System.Windows.Forms.Label();
            this.btnOdgovor2 = new System.Windows.Forms.Button();
            this.btnOdgovor3 = new System.Windows.Forms.Button();
            this.btnOdgovor1 = new System.Windows.Forms.Button();
            this.btnIzlaz = new System.Windows.Forms.Button();
            this.cmbBrojPitanja = new System.Windows.Forms.ComboBox();
            this.lblBrojPitanja = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bntNovaIgra
            // 
            this.bntNovaIgra.Location = new System.Drawing.Point(15, 395);
            this.bntNovaIgra.Name = "bntNovaIgra";
            this.bntNovaIgra.Size = new System.Drawing.Size(93, 53);
            this.bntNovaIgra.TabIndex = 0;
            this.bntNovaIgra.Text = "Nova igra";
            this.bntNovaIgra.UseVisualStyleBackColor = true;
            this.bntNovaIgra.Click += new System.EventHandler(this.bntNovaIgra_Click);
            // 
            // lblPitanje
            // 
            this.lblPitanje.AutoSize = true;
            this.lblPitanje.Location = new System.Drawing.Point(12, 25);
            this.lblPitanje.Name = "lblPitanje";
            this.lblPitanje.Size = new System.Drawing.Size(39, 13);
            this.lblPitanje.TabIndex = 1;
            this.lblPitanje.Text = "Pitanje";
            // 
            // btnOdgovor2
            // 
            this.btnOdgovor2.Location = new System.Drawing.Point(174, 194);
            this.btnOdgovor2.Name = "btnOdgovor2";
            this.btnOdgovor2.Size = new System.Drawing.Size(238, 56);
            this.btnOdgovor2.TabIndex = 2;
            this.btnOdgovor2.Text = "button1";
            this.btnOdgovor2.UseVisualStyleBackColor = true;
            this.btnOdgovor2.Click += new System.EventHandler(this.DatOdgovor);
            // 
            // btnOdgovor3
            // 
            this.btnOdgovor3.Location = new System.Drawing.Point(174, 256);
            this.btnOdgovor3.Name = "btnOdgovor3";
            this.btnOdgovor3.Size = new System.Drawing.Size(238, 55);
            this.btnOdgovor3.TabIndex = 3;
            this.btnOdgovor3.Text = "button2";
            this.btnOdgovor3.UseVisualStyleBackColor = true;
            this.btnOdgovor3.Click += new System.EventHandler(this.DatOdgovor);
            // 
            // btnOdgovor1
            // 
            this.btnOdgovor1.Location = new System.Drawing.Point(174, 132);
            this.btnOdgovor1.Name = "btnOdgovor1";
            this.btnOdgovor1.Size = new System.Drawing.Size(238, 56);
            this.btnOdgovor1.TabIndex = 4;
            this.btnOdgovor1.Text = "button3";
            this.btnOdgovor1.UseVisualStyleBackColor = true;
            this.btnOdgovor1.Click += new System.EventHandler(this.DatOdgovor);
            // 
            // btnIzlaz
            // 
            this.btnIzlaz.Location = new System.Drawing.Point(464, 395);
            this.btnIzlaz.Name = "btnIzlaz";
            this.btnIzlaz.Size = new System.Drawing.Size(93, 53);
            this.btnIzlaz.TabIndex = 5;
            this.btnIzlaz.Text = "Izlaz";
            this.btnIzlaz.UseVisualStyleBackColor = true;
            this.btnIzlaz.Click += new System.EventHandler(this.btnIzlaz_Click);
            // 
            // cmbBrojPitanja
            // 
            this.cmbBrojPitanja.FormattingEnabled = true;
            this.cmbBrojPitanja.Location = new System.Drawing.Point(174, 63);
            this.cmbBrojPitanja.Name = "cmbBrojPitanja";
            this.cmbBrojPitanja.Size = new System.Drawing.Size(238, 21);
            this.cmbBrojPitanja.TabIndex = 6;
            this.cmbBrojPitanja.Text = "4";
            // 
            // lblBrojPitanja
            // 
            this.lblBrojPitanja.AutoSize = true;
            this.lblBrojPitanja.Location = new System.Drawing.Point(171, 36);
            this.lblBrojPitanja.Name = "lblBrojPitanja";
            this.lblBrojPitanja.Size = new System.Drawing.Size(69, 13);
            this.lblBrojPitanja.TabIndex = 7;
            this.lblBrojPitanja.Text = "BrojPitanja??";
            // 
            // frmMilioner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 470);
            this.Controls.Add(this.lblBrojPitanja);
            this.Controls.Add(this.cmbBrojPitanja);
            this.Controls.Add(this.btnIzlaz);
            this.Controls.Add(this.btnOdgovor1);
            this.Controls.Add(this.btnOdgovor3);
            this.Controls.Add(this.btnOdgovor2);
            this.Controls.Add(this.lblPitanje);
            this.Controls.Add(this.bntNovaIgra);
            this.Name = "frmMilioner";
            this.Text = "Kviz";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bntNovaIgra;
        private System.Windows.Forms.Label lblPitanje;
        private System.Windows.Forms.Button btnOdgovor2;
        private System.Windows.Forms.Button btnOdgovor3;
        private System.Windows.Forms.Button btnOdgovor1;
        private System.Windows.Forms.Button btnIzlaz;
        private System.Windows.Forms.ComboBox cmbBrojPitanja;
        private System.Windows.Forms.Label lblBrojPitanja;
    }
}

