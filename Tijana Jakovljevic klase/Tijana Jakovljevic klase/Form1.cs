﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace Tijana_Jakovljevic_klase
{
    public partial class frmMilioner : Form
    {
        SvaPitanjaIodgovori basSve = new SvaPitanjaIodgovori();
        string putanja = Directory.GetCurrentDirectory();
        int brojOdgPitanja;
        public frmMilioner()
        {
            InitializeComponent();
            cmbBrojPitanja.Items.Add(5);
            cmbBrojPitanja.Items.Add(10); cmbBrojPitanja.Items.Add(15); cmbBrojPitanja.Items.Add(20); cmbBrojPitanja.Items.Add(25);
            lblPitanje.Visible = btnOdgovor1.Visible = btnOdgovor2.Visible = btnOdgovor3.Visible = false;
        }

        private void bntNovaIgra_Click(object sender, EventArgs e)
        {
            Excel.Application aplikacija = new Excel.Application();
            Excel.Workbook radnaKnjiga = aplikacija.Workbooks.Open(putanja + @"\" + "pitanja.xlsx");
            Excel.Worksheet radniList = radnaKnjiga.Sheets[1];
            Excel.Range domet = radniList.UsedRange;
            brojOdgPitanja = 0;
            PitanjaOdgovori.BrojBodova = 0;

            lblPitanje.Visible = btnOdgovor1.Visible = btnOdgovor2.Visible = btnOdgovor3.Visible = true;

            SvaPitanjaIodgovori.brojIzabranihPitanja = int.Parse(cmbBrojPitanja.Text);

            lblBrojPitanja.Visible = cmbBrojPitanja.Visible = false;

            basSve.PrebrojPitanjaIPopuniListe(domet);

            PitanjaOdgovori piodg = new PitanjaOdgovori();
            basSve.PredajPitanjeIodg(piodg);

            piodg.PromesajOdgovore();

            lblPitanje.Text = piodg.Pitanje;
            btnOdgovor1.Text = piodg.Odg1;
            btnOdgovor2.Text = piodg.Odg2;
            btnOdgovor3.Text = piodg.Odg3;
            SvaPitanjaIodgovori.TacanOdg = piodg.UvekTacan;

            radnaKnjiga.Close();
            aplikacija.Quit();
            radnaKnjiga = null;
            GC.Collect();
        }

        private void DatOdgovor(object sender, EventArgs e)
        {
            if (brojOdgPitanja == 0)
            {
                System.Windows.Forms.Button b = (System.Windows.Forms.Button)sender;
                if (b.Text == SvaPitanjaIodgovori.TacanOdg)
                {
                    PitanjaOdgovori.BrojBodova++;
                }
                MessageBox.Show(PitanjaOdgovori.BrojBodova.ToString());
                PitanjaOdgovori piodg = new PitanjaOdgovori();
                basSve.PredajPitanjeIodg(piodg);
                piodg.PromesajOdgovore();
                SvaPitanjaIodgovori.TacanOdg = piodg.UvekTacan;
                lblPitanje.Text = piodg.Pitanje;
                btnOdgovor1.Text = piodg.Odg1;
                btnOdgovor2.Text = piodg.Odg2;
                btnOdgovor3.Text = piodg.Odg3;
                brojOdgPitanja++;
            }
            else if (SvaPitanjaIodgovori.brojIzabranihPitanja - 1 == brojOdgPitanja)
            {
                System.Windows.Forms.Button b = (System.Windows.Forms.Button)sender;
                if (b.Text == SvaPitanjaIodgovori.TacanOdg)
                {
                    PitanjaOdgovori.BrojBodova++;
                }
                MessageBox.Show("Ukupan broj bodova je: " + PitanjaOdgovori.BrojBodova.ToString());
                lblPitanje.Visible = btnOdgovor1.Visible = btnOdgovor2.Visible = btnOdgovor3.Visible = false;
                brojOdgPitanja++;

            }
            else
            {
                System.Windows.Forms.Button b = (System.Windows.Forms.Button)sender;
                if (b.Text == SvaPitanjaIodgovori.TacanOdg)
                {
                    PitanjaOdgovori.BrojBodova++;
                }
                MessageBox.Show(PitanjaOdgovori.BrojBodova.ToString());
                PitanjaOdgovori piod = new PitanjaOdgovori();
                basSve.PredajPitanjeIodg(piod);
                piod.PromesajOdgovore();
                SvaPitanjaIodgovori.TacanOdg = piod.UvekTacan;
                lblPitanje.Text = piod.Pitanje;
                btnOdgovor1.Text = piod.Odg1;
                btnOdgovor2.Text = piod.Odg2;
                btnOdgovor3.Text = piod.Odg3;
                brojOdgPitanja++;
            }
        }

        private void btnIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
