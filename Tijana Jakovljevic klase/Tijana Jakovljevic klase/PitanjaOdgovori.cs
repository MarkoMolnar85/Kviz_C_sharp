﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tijana_Jakovljevic_klase
{
    class PitanjaOdgovori
    {
        static int brojBodova;

        public static int BrojBodova
        {
            get { return PitanjaOdgovori.brojBodova; }
            set { PitanjaOdgovori.brojBodova = value; }
        }
        string pitanje;
        Random rand = new Random(DateTime.Now.Millisecond);
        public string Pitanje
        {
            get { return pitanje; }
            set { pitanje = value; }
        }
        string uvekTacan;

        public string UvekTacan
        {
            get { return uvekTacan; }
            set { uvekTacan = value; }
        }
        string odg1;

        public string Odg1
        {
            get { return odg1; }
            set { odg1 = value; }
        }
        string odg2;

        public string Odg2
        {
            get { return odg2; }
            set { odg2 = value; }
        }
        string odg3;

        public string Odg3
        {
            get { return odg3; }
            set { odg3 = value; }
        }
        public void PromesajOdgovore()
        {
            int index = 0;
            string[] nasumicni = new string[3];
            nasumicni[0] = odg1;
            nasumicni[1] = odg2;
            nasumicni[2] = odg3;
           
            index = rand.Next(0, 3);
            int cuvaindex1 = index;
            odg1 = nasumicni[index];

            do
            {
                index = rand.Next(0, 3);
            } while (index == cuvaindex1);
            odg2 = nasumicni[index];
            int cuvaindex2 = index;

            do
            {
                index = rand.Next(0, 3);
            } while (index == cuvaindex1 || index == cuvaindex2);
            odg3 = nasumicni[index];
            
            nasumicni = null;

        }

    }
}
